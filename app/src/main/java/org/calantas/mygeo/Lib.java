/**  
License: GPL-3.0-or-later
Author: veto@myridia.com
Description: Collection of reused methods for this app
*/
package org.calantas.mygeo;
import android.content.Intent;
import android.content.Context;
import android.location.Location;
import android.widget.EditText;
import android.text.Selection;
import android.text.Layout;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.media.Ringtone;
import android.view.View;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import android.app.Dialog;
import android.widget.Button;  
import android.widget.TextView;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Date;
import java.text.SimpleDateFormat;
import android.support.v4.text.HtmlCompat;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.util.Log;
import com.google.gson.Gson; 
import com.google.gson.GsonBuilder;
import java.util.Hashtable;
import java.util.Dictionary;
import android.preference.PreferenceManager;
import android.content.res.Resources;

public class Lib extends Main 
{
   private final Context context;
   Dialog dialog;

   public Lib(Context context)
   {
     this.context = context;

   }

  public Boolean drop_menu(MenuItem item, Context context, EditText msg  )
  {
    switch (item.getItemId())
    {
      case R.id.main:
        Intent intent_main = new Intent(context, Main.class);
        intent_main.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.context.startActivity(intent_main);
        return true;
      case R.id.settings:
        Intent intent_setting = new Intent(context, Setting.class);
        intent_setting.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.context.startActivity(intent_setting);
        return true;
      case R.id.edit_template:
        Intent intent_template = new Intent(context, Editor.class);
        intent_template.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.context.startActivity(intent_template);
        return true;
      case R.id.about:
        create_dialog(context);
        return true;
      case R.id.contact:
        send_email();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }

  }


  public Boolean get_alert(Context context)
  {
    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
    Boolean r = pref.getBoolean("alert", false);
    return r;
  }

  public String get_saved_date_data(Context context)
  {
    Resources res = context.getResources();
    SharedPreferences pref = context.getSharedPreferences(res.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    String date = pref.getString("date", "");
    return date;
  }


  public void save_date_data(Context context)
  {
    String date = get_date(context);
    Resources res = context.getResources();
    SharedPreferences pref = context.getSharedPreferences(res.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = pref.edit();
    editor.putString("date", date);
    editor.commit();
  }



  public String get_date(Context context)
  {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    String b = prefs.getString("date_format", "1");
    Resources res = context.getResources();
    String[] c = res.getStringArray(R.array.date_format);
    Integer index = Integer.valueOf(b)-1;
    String date_format = String.valueOf(c[index]);
    Long t = System.currentTimeMillis();
   SimpleDateFormat sdf = new SimpleDateFormat(date_format);
   //SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
   Date _date = new Date(t);
   return sdf.format(_date);
  }


 public void logme(String message, EditText msg)
  {
    String[] lines = msg.getText().toString().split("\n");
    int c = lines.length;
    String line = message ;
    if(c < 100)
    {
      line = '\n' + line +  msg.getText();
    }  
    msg.setText(line);
    Log.e("mygeo",message);
  }


 public String send_email()
 {
   String r = "...send email \n";
   String[] TO = {"mygeo@calantas.org"};
   String[] CC = {""};
   Intent emailIntent = new Intent(Intent.ACTION_SEND);
      
   emailIntent.setData(Uri.parse("mailto:"));
   emailIntent.setType("text/plain");
   emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
   emailIntent.putExtra(Intent.EXTRA_CC, CC);
   emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Message from myGeo App");
   emailIntent.putExtra(Intent.EXTRA_TEXT, "Hey from myGeo App");
      
   try
   {
     r = r + "... try send email \n";
     Intent intent = Intent.createChooser(emailIntent, "Send mail...");
     intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
     this.context.startActivity(intent);
     //finish();
   }
   catch (android.content.ActivityNotFoundException ex)
   {
     r = r + "...failed  send email \n";
   }
    return r;
}


    public void create_dialog(Context self)
    {
      dialog = new Dialog(self);

      dialog.setTitle("About");
      dialog.setContentView(R.layout.about);

      String t = get_about();
      WebView browser = (WebView) dialog.findViewById(R.id.about_html);
      browser.loadData(t, "text/html","UTF-8");
      browser.setBackgroundColor(0x00000000);

      Button close_btn = (Button) dialog.findViewById(R.id.btn_close);

      close_btn.setOnClickListener(new View.OnClickListener()
      {
	public void onClick(View v)
        {
	  dialog.dismiss();
        }
      });
      dialog.show();
    }


  /**
   @param Edit Text Oject
   @param String placeholder
   @return void with update the the Text Object
  */
  public void insert_into_editText(EditText e, String text) { 
    String etext = e.getText().toString();
    int index = Math.max(e.getSelectionStart(), 0); 
    StringBuilder new_text = new StringBuilder(etext); 
    new_text.insert(index, text);
    e.setText(new_text.toString());  
    e.setSelection(index + 3);
   }  

  public String get_template()
  {
    InputStream fr = this.context.getResources().openRawResource(R.raw.template);
    return get_text(fr);
  }


  public String get_about()
  {
    InputStream fr = this.context.getResources().openRawResource(R.raw.about);
    return get_text(fr);
  }

  public String get_text(InputStream fr)
  {
    StringBuilder sb = new StringBuilder();
    String string = "";
    BufferedReader br = new BufferedReader(new InputStreamReader(fr));
    while (true) 
    {
      try 
      {
        if ((string = br.readLine()) == null) break;
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
        sb.append(string).append("\n");
      }
      return sb.toString();
  }




  public static String remove_last_line(String s)
  {
    int index = s.lastIndexOf("\n");
    if (index < 0)
    {
      return s;
    }
    else
    {
      return s.substring(0, index);
    }
  }


}
