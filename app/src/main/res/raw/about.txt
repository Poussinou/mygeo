<html>
  <style>
  body {
  text-align:left;
  color:white;
  font-size: 0.9em;
  }
  a {
  color: white;
  }

dl { position: relative; }
dl dt { width: 5em; position: absolute; left: 0; }
dl dd { margin-left: 5em; }


  </style>

  <body >
    <dl>
      <dt>Name:</dt><dd>myGeo</dd>
      <dt>License:</dt><dd>GPL-3.0-or-later</dd>
      <dt>Desc:</dt><dd>get your lat and lon cords</dd>
      <dt>Author:</dt><dd><a href:"mailto:veto@myridia.com">veto@myridia.com</a></dd>
      <dt>Repository:</dt><dd><a href="https://calantas.org/mygeo">https://calantas.org/mygeo</a></dd>

</html>


